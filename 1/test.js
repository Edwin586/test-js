// Fichero modificado de acuerdo a lo solicitado - Edwin Fuentes
var rgb = {
    red: "#FF0000",
    green: "#00FF00",
    blue: "#0000FF"
};

var wb = {
    white: "#FFFFFF",
    black: "#000000"
};
const copyRgb = {...rgb}

var colors = Object.assign(copyRgb, wb);

//Parte te codigo para IE -->
var objs = [rgb, wb],
    colorsIE =  objs.reduce(function (r, o) {
        Object.keys(o).forEach(function (k) {
            r[k] = o[k];
        });
        return r;
    }, {});

