#Test 1 - Respuestas 

1.Valor de las variables al finalizar el script:

rgb = {red: "#FF0000", green: "#00FF00", blue: "#0000FF", white: "#FFFFFF", black: "#000000"}
wb = {white: "#FFFFFF", black: "#000000"}
colors = {red: "#FF0000", green: "#00FF00", blue: "#0000FF", white: "#FFFFFF", black: "#000000"}

2.Se creó una copia de la variable 'rgb' ('copyRgb'). En el método Object.assign cambiamos el parametro 'rgb' por la copia ('copyRgb'), con esto mantenemos los valores iniciales de la variable 'rgb'

Código:

var rgb = {
    red: "#FF0000",
    green: "#00FF00",
    blue: "#0000FF"
};

const copyRgb = {...rgb}
var wb = {
    white: "#FFFFFF",
    black: "#000000"
};

var colors = Object.assign(copyRgb, wb);

3. El método Object.assing no es compatible con el navegador IE. Por lo tanto debemos iterar un array que contengan nuestros dos objetos, para esto utilizamos el método (array.reduce) que nos permite ejecutar una función que nos permitirá iterar nuestro objetos 'rgb' y 'wb'. Para despues retornar el nuevo objeto combinado.

Código:

var objs = [rgb,wb],
    colors =  objs.reduce(function (r, o) {
         Object.keys(o).forEach(function (k) {
            r[k] = o[k];
         });
        return r;
    },{});

**PD** En el fichero test.js Subí los ajustes de código

Firma: Edwin Fuentes - Front end Dev