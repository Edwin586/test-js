export default class EventManager{
    constructor(types,events){ //Inicializamos nuestro constructor
        this.types = types;
        this.events = events;
    }
    run() {
        // implement your code here...
        for (const key in this.types) {//iteramos sobre nuestro objeto obtenido
            if (this.types.hasOwnProperty(key)) {
                if (this.types[key].second !==5) {
                    const { second, type, message } = this.types[key]; 
                    setTimeout(function () { //seteamos nuestro cronómetro
                        console.log(`At second ${second}: ${ JSON.stringify({type, message}) }`);
                    }, second * 1000) // aumentamos el tiempo de acuerdo a la posición del contador
                }
                
            }
        }
    }
};