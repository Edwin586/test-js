import EventManager from './EventManager';
import Event from './Event';

export default class EventManagerFactory{
    static create(events, types) {
        
        return new EventManager(events, types);//pase los parametros 'events' y  'types' a nuestra Clase EventManager
    }
};