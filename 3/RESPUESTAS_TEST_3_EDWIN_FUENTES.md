# Test 3 - Respuestas 

Si queremos que este código se ejecute en IE, debemos utilizar una librería que nos permita interpretar promesas para versiones antiguas de navegadores (como Bluebird), incluir esta librería, cambiar su codificación y usar las estructuras de codificación ES5 (sin funciones de flecha, sin let, etc.) para que se pueda ejecutar en los navegadores antiguos.

O bien, puede usar un transpilador (como Babel) para convertir su código ES6 a código ES5 que funcionará en navegadores más antiguos.

Para este caso utilice la librería BlueBird, y codifiqué el script a version ES5 