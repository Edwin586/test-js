# Test 2-Respuestas

1.EL valor por consola que imprime es 5 veces el numero 5. 
El motivo por el cual imprime este numero, tiene que ver con el acceso de la variable del for (i) en nuestro codigo,  en este caso un acceso global (global scope), por lo tanto hace un conteo hasta llegar a la posición 5 y despues entra a nuestra funcion de setTimeout. Hay varias formas de arreglar esto; una es con closures o declarar nuestra variable "i" con la palabra reservada 'let' estas formas nos permite trabajar con variables locales y evitar que javascript las interprete de manera global asegurandonos de que solo esas variables se utilicen en ese contexto. Si dejamos el código de esta manera nos mostrará todos los valores al tiempo no despues de cada segundo, para arreglarlo multiplicamos la posicion donde se encuentre el ciclo por el valor de delay de la función setTimeout (i *1000).

Fragmento de código solucionado:

for (let i = 0; i < 5; i++) { // nos aseguramos en declarar la variable con la palabra reservada 'let'
    setTimeout(function () {
        console.log(i);
    }, i * 1000) // aumentamos el tiempo de acuerdo a la posición del contador
}